# 1. Create 5 variables and output them in the command prompt in the following format: 

name = "Jose"
age = 38
occupation = "writer"
movie = "One More Chance"
decimal = 99.6

print(f"I am {name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {decimal} %")

# 2. Create 3 variables, num1, num2, and num3
# a. Get the product of num1 and num2
# b. Check if num1 is less than num3
# c. Add the value of num3 to num2

num1 = 5
num2 = 10
num3 = 20

print(num1 * num2)
print(num1 <= num3)
print(num3 + num2)